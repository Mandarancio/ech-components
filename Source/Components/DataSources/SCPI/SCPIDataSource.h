/**
 * @file SCPIDataSource.h
 * @brief Header file for class SCPIDataSource
 * @date 22/02/2022
 * @author Martino FERRARI
 *
 * @copyright Copyright 2022 ITER.
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved
 * by the European Commission - subsequent versions of the EUPL (the "Licence")
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
 *
 * @warning Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS IS"
 * basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the Licence permissions and limitations under the Licence.
 *
 * @details This header file contains the declaration of the class
 * SCPIDataSource with all of its public, protected and private members. It may
 * also include definitions for inline methods which need to be visible to the
 * compiler.
 */

#ifndef SCPIDATASOURCE_H_
#define SCPIDATASOURCE_H_

/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/

#include "ConfigurationDatabase.h"
#include "DataSourceI.h"
#include "EmbeddedServiceI.h"
#include "EmbeddedServiceMethodBinderI.h"
#include "ErrorType.h"
#include "ExecutionInfo.h"
#include "MessageI.h"
#include "SCPITools.h"
#include "SingleThreadService.h"
#include "StructuredDataI.h"
#include "scpi.h"
#include "types.h"

/*---------------------------------------------------------------------------*/
/*                           Class declaration                               */
/*---------------------------------------------------------------------------*/
namespace MARTe {
/**
 * @brief A DataSource which allows to retrieve and send data from any number of
 * instrumentation signals using the SCPI protocol. Data is asynchronously
 * retrieved using ca_create_subscriptions in the context of a
 * different thread (w.r.t. to the real-time thread).
 *
 * The configuration syntax is (names are only given as an example):
 *
 * <pre>
 * +RTMSCPIInput = {
 *     Class = SCPI::SCPIDataSource
 *     StackSize = 1048576 // Optional the EmbeddedThread stack size.
 *     CPUs = 0xff // Optional the affinity of the EmbeddedThread.
 *     URI = tcp://127.0.0.1:5025 // protocol://path:options (tcp/uart)
 *     Timeout = 1000 // Communication timeout (default=0 [infinite])
 *     Delay = 100 // Optional delay in ms between comands (default=0ms)
 *     MaxTries = 1 // Optional maximum number of tries before giving up (default=0 [infinite])
 *     Readback = false // Optional enable/disable readback for the writing (default=true)
 *     Commands = {
 *         configure = {                     // command name
 *            Signals = {                    // ordered set of signal to set with optional value
 *               voltage = value             // signal_id = value|constant::VALUE
 *               output = constant::off      // example with fixed value
 *            }
 *            Trigger =  settingTrigger      // trigger signal to send command
 *            Priority = 1                   // command prioruty
 *            Delay = 1000                   // minimum delay between commands
 *                                           // (default = global)
 *         }
 *     }
 *     Signals = {
 *         DriverStatus = {       // Current driver status.
 *            Type = uint32       // bit set representing status and error code.
 *                                // first 8 bit status, second 8 error_code.
 *         }
 *         settingTrigger = {
 *            Type = uint8        // uint8 only
 *            Trigger = RISE      // RISE|FALL|HIGH|LOW
 *         }
 *         voltage = { // At least one shall be defined
 *             Signal = MEAS:VOLT // SCPI signal name
 *             Type = float32     // Compulsory. Supported types are char8[64],
 *                                // bool, int32,
 *                                // uint32, float32 and void
 *             Monitor = auto     // monitoring mode: auto|force
 *             Direction = INPUT  // Signal direction: INPUT|OUTPUT
 *          }
 *          ...
 *     }
 * }
 * </pre>
 */
class SCPIDataSource : public DataSourceI, public EmbeddedServiceMethodBinderI {
public:
    CLASS_REGISTER_DECLARATION()

    /**
     * @brief Constructor. NOOP.
     */
    SCPIDataSource();

    /**
     * @brief Desctructor. Frees the memory structure and disconnect host
     */
    virtual ~SCPIDataSource();

    /**
     * @brief See DataSourceI::GetNumberOfMemoryBuffers.
     * @details Only InputSignals are supported.
     * @return MemoryMapInputBroker.
     */
    virtual const char8* GetBrokerName(StructuredDataI& data,
        const SignalDirection direction);
    /**
     * @see DataSourceI::Initialise
     * @details Initialise the parameters of the SCPIDataSource
     **/
    virtual bool Initialise(StructuredDataI& data);

    /**
     * @brief Allocates the MemoryDataSourceI memory buffer and spawns the thread
     * if in IndependentThread mode.
     * @return true if MemoryDataSourceI::AllocateMemory returns true and the
     * Service is successfully started (in IndependentThread mode).
     */
    virtual bool AllocateMemory();

    /**
     * @brief See DataSourceI::GetSignalMemoryBuffer.
     * @pre
     *   SetConfiguredDatabase
     */
    virtual bool GetSignalMemoryBuffer(const uint32 signalIdx,
        const uint32 bufferIdx,
        void*& signalAddress);
    /**
     * @see MemoryDataSourceI::PrepareNextState
     * @details Sets the member values of signalFlags for each signal storing
     * if the signal has to be read, write.
     */
    virtual bool PrepareNextState(const char8* const currentStateName,
        const char8* const nextStateName);

    /**
     * @see MemoryDataSourceI::SetConfiguredDatabase
     * @details Creates the SCPI operators for each signal looking at
     * the configured signal type.
     */
    virtual bool SetConfiguredDatabase(StructuredDataI& data);

    /**
     * @brief Gets the affinity of the thread which is going to be used to
     * asynchronously read data from the ca_create_subscription.
     * @return the the affinity of the thread which is going to be used to
     * asynchronously read data from the ca_create_subscription.
     */
    uint32 GetCPUMask() const;

    /**
     * @brief Gets the stack size of the thread which is going to be used to
     * asynchronously read data from the ca_create_subscription.
     * @return the stack size of the thread which is going to be used to
     * asynchronously read data from the ca_create_subscription.
     */
    uint32 GetStackSize() const;

    /**
     * @brief Provides the context to execute all the EPICS relevant calls.
     * @details Executes in the context of a spawned thread the following EPICS
     * calls: ca_context_create, ca_create_channel, ca_create_subscription,
     * ca_clear_subscription, ca_clear_event, ca_clear_channel, ca_detach_context
     * and ca_context_destroy
     * @return ErrorManagement::NoError if all the EPICS calls return without any
     * error.
     */
    virtual ErrorManagement::ErrorType Execute(ExecutionInfo& info);

    /**
     * @brief See DataSourceI::Synchronise.
     * @return true.
     */
    virtual bool Synchronise();

    /**
     * @brief See DataSourceI::GetOutputBrokers.
     * @return true.
     */
    bool GetOutputBrokers(ReferenceContainer& outputBrokers,
        const char8* functionName, void* const gamMemPointer);

private:
    /**
     * The CPU mask for the executor
     */
    uint32 cpuMask;

    /**
     * The stack size
     */
    uint32 stackSize;
    SingleThreadService executor;

    /*Global parameters */
    uint32 timeout; // Communication layer timeout setting.
    uint64 delay; // Delay between commansd in ms.
    uint64 timestamp_us; // Last timestamp in us.
    bool readback; // readback flag for writing.
    /**
     * Signal configuration
     **/
    ConfigurationDatabase originalSignalsDatabase; // Signals database
    ConfigurationDatabase originalCommandsDatabase; // Command database
    bool hasCommands; // command flag for configuration.

    scpi::DeviceManager* deviceManager; // Device driver.

    uint32 driverStatus; // Driver status signal.
    Signals* signals; // Full list of pointers.
    TriggerWrapper* triggerList; // List of trigger signals.
    uint32 triggerNum; // Number of trigger siganls.
    SCPIWrapper* signalList; // List of SCPI signals.
    uint32 signalNum; // Numer of SCPI signals.
    SCPICommand* commands; // List of commands.
    uint32 commandNum; // Number of commands.

    uint32* autoMonitorList; // List of signals in the auto/scan monitor mode.
    uint32 autoMonitorLen; // Number of signals in the auto/scan monitor mode.
    uint32 autoMonitorCounter; // Current scan signal to monitor.
    uint32* forceMonitorList; // List of signals in the force monitor mode.
    uint32 forceMonitorLen; // Number of signals in the force monitor mode.
    uint32 forceMonitorCounter; // Current forced signal.
    uint32 currentSignalID; // Current signal identifier.

    SCPICommand* currentCommand; // Current command being executed (if any).
    uint32 commandPosition; // Command counter.
    bool commandStarted; // Command started flag.
    /*******************
     * PRIVATE METHODS *
     *******************/

    /**
     * @brief Count number of SCPI signals in the list of signal.
     **/
    uint32 countSignals(bool& ok);
    /**
     * @brief Configure the i-th SCPI signal from the configuration database.
     **/
    bool loadSignal(SCPIWrapper& signal, bool& ok, uint32 i);

    /**
     * @brief Count number of trigger signals in the list of signal.
     **/
    uint32 countTriggers(bool& ok);

    /**
     * @brief Configure the i-th trigger signal from the configuration database.
     **/
    bool loadTrigger(TriggerWrapper& trigger, bool& ok, uint32 i);

    /**
     * @brief Configure the commands using the commands database.
     **/
    void configureCommands();

    /**
     * @brief Get SCPI signal index from signal name.
     * @return Signal index if found, -1 otherwise.
     **/
    int32 getSignalIndex(const char8* str);

    /**
     * @brief Get trigger signal index from signal name.
     * @return Signal index if found, -1 otherwise.
     **/
    int32 getTriggerIndex(const char8* str);

    /**
     * @brief Execute current command loop.
     * @param  status current driver status.
     * @return Execution error.
     **/
    ErrorManagement::ErrorType executeCommand(scpi::cstatus_t status);
    /**
     * @brief Execute monitor loop for state DONE.
     * @return Execution error.
     **/
    ErrorManagement::ErrorType executeDone();
    /**
     * @brief Execute monitor loop for state BUSY.
     * @return Execution error.
     **/
    ErrorManagement::ErrorType executeBusy();
    /**
     * @brief execute monitor loop for state READY.
     * @return Execution error.
     **/
    ErrorManagement::ErrorType executeReady();
    /**
     * @brief execute monitor loop for state ERROR.
     * @return Execution error.
     **/
    ErrorManagement::ErrorType executeError();
};
}; // namespace MARTe
#endif // SCPIDATASOURCE_H_
