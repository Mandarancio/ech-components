#include "SCPIDataSource.h"
#include "AdvancedErrorManagement.h"
#include "AnyType.h"
#include "CString.h"
#include "CompilerTypes.h"
#include "ConfigurationDatabase.h"
#include "DataSourceI.h"
#include "EmbeddedServiceMethodBinderI.h"
#include "ErrorType.h"
#include "ExecutionInfo.h"
#include "GlobalObjectsDatabase.h"
#include "LinkedListHolder.h"
#include "MemoryMapInputBroker.h"
#include "MemoryMapSynchronisedOutputBroker.h"
#include "MessageI.h"
#include "Reference.h"
#include "SCPITools.h"
#include "StreamString.h"
#include "StringHelper.h"
#include "TypeDescriptor.h"
#include "Vector.h"
#include "ZeroTerminatedArray.h"
#include "device.h"
#include "scpi.h"
#include "types.h"
#include <cstring>
#include <sys/time.h>
#include <unistd.h>

namespace MARTe {

/**
 * @brief Convert MARTe TypeDescriptor to scpi::atype_t.
 * @return Converted type, T_ERR if not recognized.
 **/
scpi::atype_t t2at(TypeDescriptor td)
{
    if (td == MARTe::UnsignedInteger32Bit)
        return scpi::T_U32;
    if (td == MARTe::SignedInteger32Bit)
        return scpi::T_I32;
    if (td == MARTe::UnsignedInteger8Bit)
        return scpi::T_B08;
    if (td == MARTe::Float32Bit)
        return scpi::T_F32;
    if (td == MARTe::VoidType)
        return scpi::T_CMD;
    if (td == MARTe::CharString)
        return scpi::T_STR;
    return scpi::T_ERR;
}

/**
 * @brief Copy scpi::Value to MARTe::AnyType.
 * @return True if the operation was successfull, false otherwise.
 **/
bool v2p(scpi::Value v, void* val)
{
    switch (v.type) {
    // 4 Bytes types.
    case (scpi::T_I32):
    case (scpi::T_U32):
    case (scpi::T_F32):
        std::memcpy(val, &v.value, 4);
        return true;
    // 1 Byte types
    case (scpi::T_B08):
        std::memcpy(val, &v.value, 1);
        return true;
        // 64 bytes string
    case (scpi::T_STR):
        std::memcpy(val, v.value.str, scpi::MAX_STR_LEN);
        return true;
        // Void type
    case (scpi::T_CMD):
        return true;
    default:
        return false;
    }
}

/**
 * @brief Package SCPIWrapper value to a scpi::Value.
 * @return Packaged scpi::Value.
 **/
scpi::Value p2v(SCPIWrapper* parameter)
{
    scpi::Value value;
    value.type = t2at(parameter->td);
    value.value = parameter->value;
    return value;
}

const char8* getConstantValue(const char8* string, bool& ok)
{
    if (!StringHelper::CompareN(string, "constant::", 10)) {
        uint32 len = StringHelper::Length(string);
        char8* result = new char8[len - 10];
        ok = StringHelper::Substr(10, len - 1, string, result);
        if (ok)
            return result;
        else
            delete[] result;
    } else if (!StringHelper::Compare(string, "value")) {
        ok = true; // variable value nothing to do.
    } else {
        // something wrong here!
        ok = false;
    }
    return NULL_PTR(const char8*);
}

} // namespace MARTe

// Method implementation
namespace MARTe {

SCPIDataSource::SCPIDataSource()
    : DataSourceI()
    , EmbeddedServiceMethodBinderI()
    , executor(*this)
{
    // DEFAULT PARAMETER VALUES AND VOID POINTERS
    stackSize = THREADS_DEFAULT_STACKSIZE * 4u;
    cpuMask = 0xffu;
    timeout = 0u;
    readback = true;
    currentCommand = NULL_PTR(SCPICommand*);
    commandPosition = 0u;
    commandStarted = false;
    currentSignalID = 0u;
    autoMonitorLen = 0u;
    autoMonitorList = NULL_PTR(uint32*);
    autoMonitorCounter = 0u;
    forceMonitorList = NULL_PTR(uint32*);
    forceMonitorLen = 0u;
    forceMonitorCounter = 0u;
    signalList = NULL_PTR(SCPIWrapper*);
    signalNum = 0u;
    triggerList = NULL_PTR(TriggerWrapper*);
    triggerNum = 0u;
    commands = NULL_PTR(SCPICommand*);
    commandNum = 0u;
    deviceManager = NULL_PTR(scpi::DeviceManager*);
    hasCommands = false;
    signals = NULL_PTR(Signals*);
}

SCPIDataSource::~SCPIDataSource()
{
    // Stop thread.
    if (!executor.Stop()) {
        if (!executor.Stop()) {
            REPORT_ERROR(ErrorManagement::FatalError,
                "Could not stop SingleThreadedService.");
        }
    }
    // Stop driver.
    if (deviceManager != NULL_PTR(scpi::DeviceManager*)) {
        deviceManager->exit();
        delete deviceManager;
    }
    // CLEAN UP MEMORY.
    if (signalList != NULL_PTR(SCPIWrapper*)) {
        delete[] signalList;
    }

    if (triggerList != NULL_PTR(TriggerWrapper*)) {
        delete[] triggerList;
    }

    if (autoMonitorList != NULL_PTR(uint32*)) {
        delete[] autoMonitorList;
    }

    if (forceMonitorList != NULL_PTR(uint32*)) {
        delete[] forceMonitorList;
    }
    if (commands != NULL_PTR(SCPICommand*)) {
        for (uint32 i = 0u; i < commandNum; i++) {
            if (commands[i].variablesNum > 0) {
                delete[] commands[i].variables;
                delete[] commands[i].constants;
            }
        }
        delete[] commands;
    }
    if (signals != NULL_PTR(Signals*)) {
        delete[] signals;
    }
}

bool SCPIDataSource::AllocateMemory()
{
    // Start service.
    executor.SetName(GetName());
    return (executor.Start() == ErrorManagement::NoError);
}

const char8* SCPIDataSource::GetBrokerName(StructuredDataI& data,
    const SignalDirection direction)
{
    const char8* brokerName = "";
    if (direction == InputSignals) {
        brokerName = "MemoryMapInputBroker";
    } else if (direction == OutputSignals) {
        brokerName = "MemoryMapSynchronisedOutputBroker";
    }
    return brokerName;
}

bool SCPIDataSource::GetSignalMemoryBuffer(const uint32 signalIdx,
    const uint32 bufferIdx,
    void*& signalAddress)
{
    bool ok = (signals != NULL_PTR(Signals*));
    if (ok) {
        ok = (signalIdx < numberOfSignals);
    }
    if (ok) {
        if (signalIdx == 0) {
            signalAddress = &driverStatus;
            return ok;
        }
        // Use the signals list to get the correct memory pointer.
        if (signals[signalIdx - 1u].sigType == Signals::S_SCPI) {
            signalAddress = &signals[signalIdx - 1u].pointer.scpi->value;
            return ok;
        }
        signalAddress = &signals[signalIdx - 1u].pointer.trigger->value;
    }
    return ok;
}

bool SCPIDataSource::GetOutputBrokers(ReferenceContainer& outputBrokers,
    const char8* functionName,

    void* const gamMemPointer)
{

    ReferenceT<MemoryMapSynchronisedOutputBroker> broker(
        "MemoryMapSynchronisedOutputBroker");
    bool ret = broker.IsValid();
    if (ret) {
        ret = broker->Init(OutputSignals, *this, functionName, gamMemPointer);
    }
    if (ret) {
        ret = outputBrokers.Insert(broker);
    }
    return ret;
}

bool SCPIDataSource::Initialise(StructuredDataI& data)
{
    bool ret = DataSourceI::Initialise(data);
    if (ret) {
        /**
         * Global Settings
         **/
        StreamString devicePath;
        ret = data.Read("URI", devicePath); // Device URI
        if (ret) {
            AnyType parameter;
            uint32 maxTries = 0;
            if (!data.Read("CPUs", cpuMask))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for CPUs");
            if (!data.Read("StackSize", stackSize))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for StackSize");
            if (!data.Read("Timeout", timeout))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for Timeout");
            if (!data.Read("MaxTries", maxTries))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for MaxTries");
            if (!data.Read("Readback", readback))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for Readback");
            if (!data.Read("Delay", delay))
                REPORT_ERROR(ErrorManagement::Information,
                    "Using default value for Delay");
            deviceManager = new scpi::DeviceManager(devicePath.Buffer(), timeout, maxTries);
            if (deviceManager->status() == scpi::ERROR) {
                REPORT_ERROR(ErrorManagement::InitialisationError,
                    "SCPI Device initialization failed.");
                ret = false;
            }
        } else {
            REPORT_ERROR(ErrorManagement::InitialisationError,
                "Missing SCPI Device URI.");
        }
    }

    executor.SetStackSize(stackSize);
    executor.SetCPUMask(cpuMask);

    if (ret) {
        /**
         * Copy signals settings for further use.
         **/
        ret = data.MoveRelative("Signals");
        if (!ret) {
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Could not move to Signals sections");
            return ret;
        }
        ret = data.Copy(originalSignalsDatabase);
        if (ret)
            ret = originalSignalsDatabase.MoveToRoot();
        if (ret)
            ret = signalsDatabase.MoveRelative("Signals");
        if (ret)
            ret = signalsDatabase.Write("Locked", 1u);
        if (ret)
            ret = signalsDatabase.MoveToAncestor(1u);
    }
    if (ret) {
        /**
         * Copy commands settings (if any) for further use.
         **/
        ret = data.MoveToAncestor(1u);
        if (data.MoveRelative("Commands")) {
            ret = data.Copy(originalCommandsDatabase);
            hasCommands = ret;
            ret = data.MoveToAncestor(1u);
        } else {
            commandNum = 0;
            REPORT_ERROR(ErrorManagement::Information, "No Commands found");
            hasCommands = false;
        }
    }

    REPORT_ERROR(ErrorManagement::Information, "Intialization done");
    return ret;
}

bool SCPIDataSource::SetConfiguredDatabase(StructuredDataI& data)
{
    /**
     * Configure signals and commands
     **/
    bool ok = DataSourceI::SetConfiguredDatabase(data);
    ok = ok && (numberOfSignals > 0u);
    if (ok) {
        // CONFIGURE SIGNALS
        // Ensure that first signal is called `driverStatus` and
        // its type is uint32

        if (StringHelper::Compare(originalSignalsDatabase.GetChildName(0u), "DriverStatus")) {
            REPORT_ERROR(ErrorManagement::FatalError, "First signal must be named `DriverStatus` and typed `uint32`");
            return false;
        }
        if (GetSignalType(0u) != MARTe::UnsignedInteger32Bit) {
            REPORT_ERROR(ErrorManagement::FatalError, "Signal `DriverStatus` must be typed `uint32`.");
            return false;
        }

        // Create list of generic signals pointer.
        signals = new Signals[numberOfSignals - 1];
        // Count number of SCPI signals.
        signalNum = countSignals(ok);
        if (!ok)
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Impossible to count number of signals.");
        // Count number of trigger signals.
        triggerNum = countTriggers(ok);
        if (!ok)
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Impossible to count number of trigger signals.");

        // initialize list of SCPI and trigger signals.
        signalList = new SCPIWrapper[signalNum];
        triggerList = new TriggerWrapper[triggerNum];
        uint32 iSig = 0u; // current signal counter.
        uint32 iTrig = 0u; // current trigger counter.
        // configure all signals.
        for (uint32 i = 1u; (i < numberOfSignals) && ok; i++) {
            StreamString signalName;
            ok = GetSignalName(i, signalName);
            if (ok) {
                ok = originalSignalsDatabase.MoveRelative(signalName.Buffer());
            } else
                REPORT_ERROR(ErrorManagement::Warning, "Missing signal named %d", i);
            StreamString parameterPath;
            if (ok) {
                // Check if signal is SCPI or Trigger (depending if the field `Signal`
                // is configured.)
                if (originalSignalsDatabase.Read("Signal", parameterPath)) {
                    // Try to load SCPI signal.
                    if (loadSignal(signalList[iSig], ok, i)) {
                        // Configure signal type and pointer.
                        signals[i - 1u].sigType = Signals::S_SCPI;
                        signals[i - 1u].pointer.scpi = &signalList[iSig];
                        iSig++;
                    } else
                        REPORT_ERROR(ErrorManagement::ParametersError,
                            "Impossible to load signal %d", i);
                } else {
                    // Try to load trigger signal.
                    if (loadTrigger(triggerList[iTrig], ok, i)) {
                        // Configure signal type and pointer.
                        signals[i - 1u].sigType = Signals::S_TRIGGER;
                        signals[i - 1u].pointer.trigger = &triggerList[iTrig];
                        iTrig++;
                    } else
                        REPORT_ERROR(ErrorManagement::ParametersError,
                            "Impossible to load trigger %d", i);
                }
                ok = originalSignalsDatabase.MoveToAncestor(1u);
            } else {
                REPORT_ERROR(ErrorManagement::ParametersError,
                    "Something went wrong at %d parameter named %s", i,
                    signalName.Buffer());
            }
        }
    } else {
        REPORT_ERROR(ErrorManagement::FatalError,
            "Configuring gone wrong... Number of signals: %d",
            GetNumberOfSignals());
    }
    if (ok) {
        // CONFIGURE COMMANDS
        if (hasCommands)
            configureCommands();
        // Initialize monitor lists.
        forceMonitorList = new uint32[forceMonitorLen];
        autoMonitorList = new uint32[autoMonitorLen];
        uint32 i_auto = 0u;
        uint32 i_force = 0u;
        for (uint32 i = 0u; i < signalNum; i++) {
            // Check for any input signals if the monitor mode
            // is `AUTO` or `FORCE` and assign it to the correct
            // list.
            if (signalList[i].direction == S_INPUT) {
                if (signalList[i].acquisitionMode == M_AUTO) {
                    autoMonitorList[i_auto++] = i;
                } else if (signalList[i].acquisitionMode == M_FORCE) {
                    forceMonitorList[i_force++] = i;
                }
            }
        }
    } else {
        REPORT_ERROR(ErrorManagement::FatalError, "Signal configuration failed!");
    }
    return ok;
}

ErrorManagement::ErrorType
SCPIDataSource::executeCommand(scpi::cstatus_t status)
{
    ErrorManagement::ErrorType err = ErrorManagement::NoError;
    // exec command
    if (!commandStarted) {
        deviceManager->reset(); // reset communication to ensury ready state
        commandStarted = true; // set started flag
        commandPosition = 0u; // reset command counter
    } else if (status == scpi::DONE) {
        // if the command is already started and the driver state is
        // done means that the current var
        deviceManager->reset(); // reset device driver to ready.
        commandPosition++; // increase command counter
        timeval now;
        gettimeofday(&now, NULL);
        timestamp_us = now.tv_sec * 1000 + now.tv_usec / 1000;
        // check if command is over [all signals set].
        if (commandPosition == currentCommand->variablesNum) {
            // DONE
            commandStarted = false;
            commandPosition = 0u;
            currentCommand = NULL_PTR(SCPICommand*);
            return err;
        }
    } else if (status != scpi::READY) { // Check if something gone wrong with last write.
        // Write unsuccessful, notify and retry...
        REPORT_ERROR(ErrorManagement::Warning,
            "Signal `%s` write was unsuccessful [%d], retry.",
            currentCommand->variables[commandPosition]->sigName,
            deviceManager->status());
        deviceManager->reset(); // reset device and try again.
    } else if (status == scpi::READY) {
        timeval now;
        gettimeofday(&now, NULL);
        uint64 current = now.tv_sec * 1000 + now.tv_usec / 1000;
        uint64 time_delay = currentCommand->delay == 0 ? delay : currentCommand->delay;
        if (current - timestamp_us < time_delay) {
            // Try to write current signal value.
            timestamp_us = current;
            SCPIWrapper* parameter = currentCommand->variables[commandPosition];
            scpi::Value val;
            if (currentCommand->constants[commandPosition].type == scpi::T_ERR) { // set variable value
                val.value = parameter->value; // Copy value
                val.type = t2at(parameter->td); // set value type
            } else {
                val = currentCommand->constants[commandPosition]; // set constant value
            }
            deviceManager->write(parameter->scpiName, val, readback); // write.
        }
    }
    return err;
}

ErrorManagement::ErrorType SCPIDataSource::executeBusy()
{
    // Wait next cycle. Nothing todo.
    return ErrorManagement::NoError;
}

ErrorManagement::ErrorType SCPIDataSource::executeReady()
{
    // driver is ready to go..
    // Get current time in ms
    timeval now;
    gettimeofday(&now, NULL);
    uint64 current_ms = now.tv_sec * 1000 + now.tv_usec / 1000;
    if (current_ms - timestamp_us >= delay) { // check if enough time is passed
        timestamp_us = current_ms;
        // Query next value.
        if (!deviceManager->query(signalList[currentSignalID].scpiName,
                t2at(signalList[currentSignalID].td))) {
            // report error if something has gone wrong
            REPORT_ERROR(ErrorManagement::Warning, "Impossible to query device");
            return ErrorManagement::CommunicationError;
        }
    }
    return ErrorManagement::NoError;
}

ErrorManagement::ErrorType SCPIDataSource::executeDone()
{
    /**
     * 4.1 Retrieve value read for scan monitor signals.
     **/
    // if driver is done, a monitor scan signal  value is ready to be
    // read.
    // uint32 id = autoMonitorList[autoMonitorCounter];
    scpi::Value value; // prepare value to be read.
    scpi::atype_t expected_type = t2at(signalList[currentSignalID].td);
    value.type = expected_type;
    time_t timestamp;
    if (deviceManager->getValue(value, timestamp)) { // get value
        timeval now;
        gettimeofday(&now, NULL);
        timestamp_us = now.tv_sec * 1000 + now.tv_usec / 1000;
        if (value.type == expected_type) { // check type corency
            signalList[currentSignalID].value = value.value; // update value
            signalList[currentSignalID].getTimestamp = timestamp; // update timestamp
            forceMonitorCounter++;
            if (forceMonitorCounter >= forceMonitorLen) {
                autoMonitorCounter = (autoMonitorCounter + 1) % autoMonitorLen; // increase monitor counter.
                currentSignalID = autoMonitorList[autoMonitorCounter];
                forceMonitorCounter = 0u;
            } else {
                currentSignalID = forceMonitorList[forceMonitorCounter];
            }
        } else {
            // datatype is different from expected, something wierd is going
            // on.
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Expected type is different from returned one (%d %d)", expected_type, value.type);
            return ErrorManagement::ParametersError;
        }
    } else {
        // Communication error
        REPORT_ERROR(ErrorManagement::CommunicationError, "Get value failed [%d].",
            deviceManager->status());
        // retry...
        deviceManager->reset();
    }
    return ErrorManagement::NoError;
}

ErrorManagement::ErrorType SCPIDataSource::executeError()
{
    REPORT_ERROR(ErrorManagement::Warning, "Query failed [%d]!",
        deviceManager->status());
    // Retry
    deviceManager->reset();
    return ErrorManagement::NoError;
}

ErrorManagement::ErrorType SCPIDataSource::Execute(ExecutionInfo& info)
{
    // Cycle execution.
    ErrorManagement::ErrorType err = ErrorManagement::NoError;
    if (info.GetStage() == ExecutionInfo::MainStage) {
        /**
         * 2. Check if device driver is not busy
         **/
        scpi::cstatus_t status = deviceManager->status();
        // If busy finish cycle.
        if (status == scpi::BUSY)
            return executeBusy();
        // monitor flag
        if (hasCommands && currentCommand != NULL_PTR(SCPICommand*)) {
            /**
             * 3. Execute command if any
             **/
            err = executeCommand(status);
        } else {
            /**
             * 4. Monitor signals if no commands is being executed.
             **/
            if (status == scpi::READY) {
                return executeReady();
            } else if (status == scpi::DONE) {
                return executeDone();
            } else {
                // Something gone wrong..
                return executeError();
            }
        }
    }

    return err;
}

bool SCPIDataSource::PrepareNextState(const char8* const currentState,
    const char8* const nextState)
{
    return true;
}

bool SCPIDataSource::Synchronise()
{
    // Check status of driver.
    if (deviceManager != NULL_PTR(scpi::DeviceManager*)) {
        driverStatus = 0u;
        scpi::cstatus_t status = deviceManager->status();
        scpi::error_t error = deviceManager->error();
        driverStatus = (1 << status) + (1 << (8 + error));
        REPORT_ERROR(ErrorManagement::Information, "Device Manager Status %d [%d], latest error %d",
            status, deviceManager->connected(), error);
    }
    /***
     * 1. Evaluate Command triggers.
     **/
    if (hasCommands) {
        uint32 currentPrio = currentCommand != NULL_PTR(SCPICommand*) ? currentCommand->priority : 0u;
        // Iterate all commands
        for (uint32 i = 0u; i < commandNum; i++) {
            if (commands[i].trigger->check()) { // evaluate trigger
                // if trigger signal is triggered and current priority is
                // less the triggered command priority set command to be
                // executed.
                REPORT_ERROR(ErrorManagement::Information, "Command %d trigger true", i);
                if (currentPrio < commands[i].priority || currentCommand == NULL_PTR(SCPICommand*)) {
                    commandStarted = false; // started flag to false
                    currentCommand = &commands[i]; // set command pointer
                    currentPrio = commands[i].priority; // current priority
                    REPORT_ERROR(ErrorManagement::Information, "Command %d triggered", i);
                }
            }
        }
    }
    return true;
}

uint32 SCPIDataSource::GetCPUMask() const { return cpuMask; }

uint32 SCPIDataSource::GetStackSize() const { return stackSize; }

uint32 SCPIDataSource::countSignals(bool& ok)
{
    // Count number of SCPI signals by check the field `Signal`
    uint32 count = 0u;
    for (uint32 i = 1u; (i < numberOfSignals) && ok; i++) {
        StreamString signalName;
        ok = GetSignalName(i, signalName);
        if (ok) {
            ok = originalSignalsDatabase.MoveRelative(signalName.Buffer());
        }
        StreamString parameterPath;
        if (ok) {
            if (originalSignalsDatabase.Read("Signal", parameterPath))
                count++;
        } else
            REPORT_ERROR(ErrorManagement::Warning, "Missing configuration");
        ok = originalSignalsDatabase.MoveToAncestor(1u);
    }
    return count;
}

uint32 SCPIDataSource::countTriggers(bool& ok)
{
    // Count number of trigger signals by checking field `Signal`.
    uint32 count = 0;
    for (uint32 i = 1u; (i < numberOfSignals) && ok; i++) {
        StreamString signalName;
        ok = GetSignalName(i, signalName);
        if (ok) {
            ok = originalSignalsDatabase.MoveRelative(signalName.Buffer());
        }
        StreamString parameterPath;
        if (ok) {
            if (!originalSignalsDatabase.Read("Signal", parameterPath))
                count++;
        } else
            REPORT_ERROR(ErrorManagement::Warning, "Missing configuration");
        ok = originalSignalsDatabase.MoveToAncestor(1u);
    }
    return count;
}

bool SCPIDataSource::loadSignal(MARTe::SCPIWrapper& signal, bool& ok,
    uint32 i)
{
    StringHelper::Copy(signal.sigName, originalSignalsDatabase.GetName());
    StreamString parameterPath;
    ok = originalSignalsDatabase.Read("Signal", parameterPath);
    if (!ok) {
        REPORT_ERROR(ErrorManagement::ParametersError, "No path defined");
        return ok;
    } else {
        StringHelper::Copy(signal.scpiName, parameterPath.Buffer());
    }
    TypeDescriptor typeDescriptor = GetSignalType(i);
    monitor_t mode = M_AUTO;
    signalDirection_t direction = S_INPUT;
    StreamString enumStr;
    if (originalSignalsDatabase.Read("Direction", enumStr)) {
        const char8* str = enumStr.Buffer();
        if (!strcmp(str, "INPUT")) {
            direction = S_INPUT;
            if (originalSignalsDatabase.Read("Monitor", enumStr)) {
                const char8* str = enumStr.Buffer();
                if (!strcmp(str, "force"))
                    mode = M_FORCE;
                else if (!strcmp(str, "auto"))
                    mode = M_AUTO;
                else {
                    REPORT_ERROR(ErrorManagement::ParametersError,
                        "Monitor mode invalid `%s`", str);
                }
            }
        } else if (!strcmp(str, "OUTPUT")) {
            direction = S_OUTPUT;
        } else if (!strcmp(str, "CONSTANT")) {
            direction = S_CONSTANT;
        } else {
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Signal direction invalid `%s`", str);
        }
    }

    if (ok) {
        if (direction == S_INPUT) {
            if (mode == M_AUTO)
                autoMonitorLen += 1;
            else if (mode == M_FORCE)
                forceMonitorLen += 1;
        }
        signal.acquisitionMode = mode;
        signal.direction = direction;
        signal.td = typeDescriptor;
        signal.getTimestamp = 0;
        signal.setTimestamp = 0;

        StringHelper::Copy(signalList[i].scpiName, parameterPath.Buffer());
    }
    return ok;
}

bool SCPIDataSource::loadTrigger(TriggerWrapper& trigger, bool& ok, uint32 i)
{
    StringHelper::Copy(trigger.sigName, originalSignalsDatabase.GetName());
    trigger.value = 0u;
    trigger.previous = 0u;
    StreamString buffer;
    if (originalSignalsDatabase.Read("Trigger", buffer)) {
        const char8* val = buffer.Buffer();
        if (!strcmp(val, "RISE"))
            trigger.mode = T_RISE;
        else if (!strcmp(val, "FALL"))
            trigger.mode = T_FALL;
        else if (!strcmp(val, "HIGH"))
            trigger.mode = T_HIGH;
        else if (!strcmp(val, "LOW"))
            trigger.mode = T_LOW;
        else {
            ok = false;
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Trigger mode [%i] not valid `%s`", i, val);
        }
    } else {
        trigger.mode = T_RISE;
    }
    return ok;
}

void SCPIDataSource::configureCommands()
{
    commandNum = originalCommandsDatabase.GetNumberOfChildren();
    commands = new SCPICommand[commandNum];
    bool ok = true;
    for (uint32 i = 0u; i < commandNum && ok; i++) {
        // Configure i-th command
        ok = originalCommandsDatabase.MoveToChild(i);
        REPORT_ERROR(ErrorManagement::Information, "Configuring command %d named %s",
            i, originalCommandsDatabase.GetName());
        StreamString string;
        // Configure trigger signal
        if (originalCommandsDatabase.Read("Trigger", string)) {
            int32 tid = getTriggerIndex(string.Buffer());
            if (tid < 0) {
                ok = false;
                REPORT_ERROR(ErrorManagement::ParametersError,
                    "Trigger signal `%s` for command `%s` not found!",
                    string.Buffer(), originalCommandsDatabase.GetName());
            } else {
                commands[i].trigger = &triggerList[tid];
            }
        } else {
            ok = false;
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Missing trigger to command %s",
                originalCommandsDatabase.GetName());
        }
        if (!originalCommandsDatabase.Read("Priority", commands[i].priority)) {
            ok = false;
            REPORT_ERROR(ErrorManagement::ParametersError,
                "Missing priority for commands %s",
                originalCommandsDatabase.GetName());
        }

        // Configure command signals
        const char8* commandName = originalCommandsDatabase.GetName();
        if (originalCommandsDatabase.MoveRelative("Signals")) {
            uint32 numSignals = originalCommandsDatabase.GetNumberOfChildren();
            commands[i].variables = new SCPIWrapper*[numSignals];
            commands[i].constants = new scpi::Value[numSignals];
            commands[i].variablesNum = numSignals;
            for (uint j = 0u; j < numSignals; j++) {
                StreamString value_str;
                const char8* sigName = originalCommandsDatabase.GetChildName(j);
                int32 sid = getSignalIndex(sigName);
                if (sid < 0) {
                    ok = false;
                    REPORT_ERROR(ErrorManagement::ParametersError,
                        "Command %s signal %s not found",
                        commandName, sigName);
                } else if (signalList[sid].direction != S_INPUT) {
                    commands[i].variables[j] = &signalList[sid];
                    if (originalCommandsDatabase.Read(sigName, value_str)) {
                        const char8* value = getConstantValue(value_str.Buffer(), ok);
                        if (!ok) {
                            REPORT_ERROR(ErrorManagement::ParametersError,
                                "Command %s:%s value `%s` is not valid",
                                commandName,
                                sigName,
                                value_str.Buffer());
                        } else {
                            if (value != NULL_PTR(const char8*)) {
                                REPORT_ERROR(ErrorManagement::Information, ">> %s:%s = %s",
                                    commandName, sigName, value);
                                commands[i].constants[j].type = t2at(signalList[sid].td);
                                ok = scpi::convert(commands[i].constants[j], value);
                                if (!ok) {
                                    REPORT_ERROR(ErrorManagement::ParametersError,
                                        "Command %s:%s impossible to convert value `%s`",
                                        commandName,
                                        sigName,
                                        value);
                                }
                                delete[] value;
                            } else {
                                commands[i].constants[j].type = scpi::T_ERR;
                                REPORT_ERROR(ErrorManagement::Information, ">> %s:%s = variable",
                                    commandName, sigName);
                            }
                        }
                    } else {
                        ok = false;
                        REPORT_ERROR(ErrorManagement::ParametersError,
                            "Command %s signal %s is not an OUTPUT signal.",
                            commandName, sigName);
                    }
                } else {
                    ok = false;
                    REPORT_ERROR(ErrorManagement::ParametersError, "Command %s impossible to configure signal %s",
                        commandName, sigName);
                }
            }
            ok = originalCommandsDatabase.MoveToAncestor(1u);
        } else {
            ok = true;
            commands[i].variables = NULL_PTR(SCPIWrapper**);
            commands[i].constants = NULL_PTR(scpi::Value*);
            commands[i].variablesNum = 0u;
            REPORT_ERROR(ErrorManagement::Information,
                "Empty signal list to command `%s`",
                originalCommandsDatabase.GetName());
        }
        // Return to list of commands
        if (ok)
            originalCommandsDatabase.MoveToAncestor(1u);
    }
}

int32 SCPIDataSource::getSignalIndex(const char8* str)
{
    for (uint32 i = 0u; i < signalNum; i++) {
        if (!strcmp(str, signalList[i].sigName))
            return static_cast<int32>(i);
    }
    return -1;
}

int32 SCPIDataSource::getTriggerIndex(const char8* str)
{
    for (uint32 i = 0u; i < triggerNum; i++) {
        if (!strcmp(str, triggerList[i].sigName))
            return static_cast<int32>(i);
    }
    return -1;
}

CLASS_REGISTER(SCPIDataSource, "1.0")
} // namespace MARTe
