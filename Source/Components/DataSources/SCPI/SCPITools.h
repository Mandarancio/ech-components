/**
 * @file SCPIInput.h
 * @brief Header file for class SCPIInput
 * @date 22/01/2022
 * @author Martino FERRARI
 *
 * @copyright Copyright 2022 ITER.
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved
 * by the European Commission - subsequent versions of the EUPL (the "Licence")
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
 *
 * @warning Unless required by applicable law or agreed to in writing,
 * software distributed under the Licence is distributed on an "AS IS"
 * basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the Licence permissions and limitations under the Licence.
 *
 * @details This header file contains the declaration of the SCPI helpers and
 * Basic types. It may also include definitions for inline methods which need
 * to be visible to the compiler.
 */

#ifndef SCPITOOLS_H_
#define SCPITOOLS_H_

/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/
#include <ctime>

/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/
#include "CompilerTypes.h"
#include "TypeDescriptor.h"
#include "types.h"
/*---------------------------------------------------------------------------*/
/*                           Class declaration                               */
/*---------------------------------------------------------------------------*/

namespace MARTe {
/**
 * Maximum size that a SCPI name may have
 */
/*lint -esym(551, MARTe::PV_NAME_MAX_SIZE) the symbol is used to define the size
 * of SCPIWrapper below*/
const uint32 SCPI_NAME_MAX_SIZE = 256u;
/**
 * Maximum size that a LXI Hostname may have
 **/
const uint32 LXI_HOST_NAME_MAX_SIZE = 256u;

/**
 * @brief Monitor mode enumerator.
 */
enum monitor_t {
    M_AUTO = 0, // Cyclically monitor the variable
    M_FORCE = 1, // Always monitor the variable
};

/**
 * @brief Signal direction enumerator.
 **/
enum signalDirection_t {
    S_INPUT, // Input variable [default]
    S_OUTPUT, // Output variable
    S_CONSTANT, // Output constant signal [value controlled by
                // commands only.
};

/**
 * @brief Command trigger mode enumerator.
 **/
enum triggerMode_t {
    T_RISE, // Trigger on signal rise [0 -> 1].
    T_FALL, // Trigger on signal fall [1 -> 0].
    T_HIGH, // Trigger on signal high [1].
    T_LOW, // Trigger on sginal low  [0].
};

/**
 * @brief Wraps for SCPI variable.
 */
struct SCPIWrapper {
    /**
     * @brief Signal name
     **/
    char8 sigName[SCPI_NAME_MAX_SIZE];

    /**
     * @brief The SCPI paramenter path.
     */
    char8 scpiName[SCPI_NAME_MAX_SIZE];

    /**
     * @brief Monitor mode.
     */
    monitor_t acquisitionMode;

    /**
     * @brief memory buffer.
     **/
    scpi::avalue_t value;

    /**
     * @brief The type descriptor.
     */
    TypeDescriptor td;

    /**
     * @brief  Latest update timestamp.
     */
    time_t getTimestamp;
    /**
     * @brief Latest set timestamp.
     **/
    time_t setTimestamp;

    /**
     * @brief signal direction.
     **/
    signalDirection_t direction;
};

/**
 * @brief Trigger Wrapper.
 **/

struct TriggerWrapper {
    /**
     * @brief Signal name (for command binding).
     **/
    char8 sigName[SCPI_NAME_MAX_SIZE];
    /**
     * @brief Current value memory buffer.
     **/
    uint8 value;
    /**
     * @brief Previusly checked value (used for edge detection).
     **/
    uint8 previous;
    /**
     * @brief Trigger mode.
     **/
    triggerMode_t mode;

    /**
     * Method to check trigger signal and move value from `value` to `previous`.
     **/
    bool check()
    {
        bool ok = false;
        if (value) {
            ok = mode == T_HIGH || (mode == T_RISE && !previous);
        } else {
            ok = mode == T_LOW || (mode == T_FALL && previous);
        }
        previous = value;
        return ok;
    }
};
/**
 * @brief Command data structure.
 */
struct SCPICommand {
    /**
     * @brief Command priority (higher the better).
     **/
    uint32 priority;
    /**
     * @brief List of pointers to the controlled signals.
     **/
    SCPIWrapper** variables;
    /**
     * @brief List of constant values.
     **/
    scpi::Value* constants;
    /**
     * @brief Number of variables controlled.
     **/
    uint32 variablesNum;
    /**
     * @brief Pointer to trigger signal.
     **/
    TriggerWrapper* trigger;
    /**
     * @brief Optional delay in ms between settings.
     **/
    uint32 delay;
};

/**
 * @brief Generic signal data structure.
 **/
struct Signals {
    /**
     * @brief Signal type.
     **/
    enum {
        S_SCPI, // SCPI signal
        S_TRIGGER, // Trigger signal
    } sigType;
    /**
     * @brief Pointer to the appropiate signal.
     **/
    union {
        SCPIWrapper* scpi; // SCPI signal pointer.
        TriggerWrapper* trigger; // Trigger signal pointer.
    } pointer;
};

}; // namespace MARTe
#endif // SCPITOOLS_H_
