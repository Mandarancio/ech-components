# SCPI Data Source

## SCPI

The *Standard Commands for Programmable Instruments*[^2] (SCPI) is a standard language to control remotely test and measurement equipment that is agnostic about the communication protocol.

### SCPI Synthax

The `SCPI` synthax is quite intuitive, the instrument as a number of registers that can be organized in sub-categories, for example:
```
  - CONFigure
    - VOLTage (RW)
    - PROTection
      - OVERVoltage (RW)
      - OVERCurrent (RW)
  - MEASure
    - VOLTage (R)
    - CURRent (R)
  - ALARMs
    - OVERVoltage (R)
    - OVERCurrent (R)
```
The register path are not case sensitive and the majuscule part represent the shortcut.
This registers can be read or write or both. The command in order to read the value of a certain registers is simply the path of the register follow by a question mark:
```
measure:voltage?
```
The device will answer with the current value of the register. The command to set the register value is also very simple, it is composed by the register path followed by
the value to set:
```
configure:voltage 10
```
The device will try to set the value but will not reply, in order to verify the register new value a new query must be done.
It is also possible to shorten up the path of the register by only using the majuscol part:
```
measure:voltage == meas:volt
```

#### Common commands
Some commands are common for all `SCPI` devices and in general they starts with a `*`, below a list of the most useful:

```
*IDN? : Get the device identifier
*CLS  : Clears all the event registers
*OPC? : Notifiy when the current operation is completed
*RST  : Reset the device
*SAV  : Save current settings to memory
*TRG  : Trigger command
*WAI  : Force to wait current command
*STB? : Read the status byte
*ESR? : Query the event status registry
```

## Communication Layers

### LXI Protocol

The *Lan eXtension for Instrumentation*[^1] offer some standard protocol in order to communicate remotely with test and measurement equipment using a standard ethernet interface.

The `LXI` implements several protocol on top of ethernet, on of this is `SCPI`.

The `LXI` standard implements a `SCPI` service over a simple `TCP` socket normally on port `5025` of the device.

### UART

Many instrumentation use `SCPI` over `UART`. This however limits the possibilities to connect multiple clients to the same interface.


## MARTe Implementation and Configuration

The MARTe `SCPIDataSource` gave the possibilities to monitor and control a `SCPI` device using both `UART` or `LXI` communication layer.
The data source cyclicaly monitor a set of given registers and asyncronously can send a set of settings to the device.
The settings are grouped in `commands` in order to ensure a certain set order as well as logical consistency (e.g. some settings sequence for turing on or off the device).

The data source use the `scpilib` code to manage the communication with the device but it implements the read/write logic.

### Configuration
For the end user the data source configuration is what matter most, below an example with all parameters explained.


```conf
+PWX1500 = {
  Class = SCPI::SCPIDataSource
  StackSize = 1048576           // EmbeddedThread stack size [OPTIONAL]
  CPUs = 0xff                   // EmbeddedThread cpu affinity [OPTIONAL]
  URI = "tcp://127.0.0.1:5025"  // SCPI Device URI protocol://path:options
  Timeout = 100                 // Communication timeout in ms (default=0, infinite) [OPTIONAL]
  Delay = 10                    // Delay in ms between consecutive commands (default=0) [OPTIONAL]
  MaxTries = 2                  // Max number of tries before giving up (default=0, infinite) [OPTIONAL]
  Readback = false              // Enable/disable readback when writing (default=true) [OPTIONAL]
  Signals = {                   // List of signals
    DriverStatus = {            // MANDATORY (both name and type)
      Type = uint32             // bit set representing the status and error code of the driver
                                // first 8 bits the status, second 8 bits the error code.
    }
    Voltage = {                 // All the other signals are user definable
      Signal = measure:voltage  // Register path
      Type = float32            // Register data type
      Direction = INPUT         // Type of register INPUT|OUTPUT|CONSTANT (default=INPUT) [OPTIONAL]
      Monitor = force           // Monioring mode auto|force (default=auto) [OPTIONAL]
    }
    OverVolage = {
      Signal = conf:prot:overv
      Type = float32
    }
    OverVoltageSet = {
      Signal = conf:prot:overv
      Type = float32
      Direction = OUTPUT
    }
    PowerOn = {
      Signal = conf:output
      Type = string
      Direction = OUTPUT
    }
    SetTrigger = {              // This is a trigger signal to send a command
      Type = uint8              // Mandatory type!
      Trigger = RISE            // Trigger logic: RISE|FALL|HIGH|LOW
    }
  }
  Commands = {                  // List of commands
    SetProtection = {
      Signals = {               // Ordered list of signals to set
        OverVoltageSet = value  // if value use the signal value
        PowerOn = constant::off // if constant:: use the defined value instead
      }
      Trigger = SetTrigger      // The name of the trigger signal
      Priority = 1              // Command priority used to manage command conflicts
      Delay = 150               // Local delay between commands (default=global delay) [OPTIONAL]
    }
  }
}
```


[^1]: LXI - the new LAND standard for networking T&M equipment, Rohde & Schwarz News, Number 190 (2006/III)    - [pdf](https://cdn.rohde-schwarz.com/pws/dl_downloads/dl_common_library/dl_news_from_rs/190/n190_lxi.pdf)
[^2]: Standard Commands for Programmable Instruments, Wikipedia - [link](https://en.wikipedia.org/wiki/Standard_Commands_for_Programmable_Instruments)
