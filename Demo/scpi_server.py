"""
Simple LXI service.

author: Martino Ferrari
email: martinogiordano.ferrari@iter.org
"""

import argparse
import logging
import numpy as np
import random
import re
import socket
import threading
import yaml

logger = logging.getLogger(__name__)


def rand() -> float:
    """Generate random number in the window (-1.0, 1.0)."""
    return 2.0 * (random.random() - 0.5)


def nrandr(mu: float = 0.0, std: float = 1.0) -> float:
    return np.random.normal(mu, std)


def is_connected(sock: socket.socket) -> bool:
    """Check if socket is connected."""
    try:
        # this will try to read bytes without blocking and also without
        # removing them from buffer (peek only)
        data = sock.recv(16, socket.MSG_DONTWAIT | socket.MSG_PEEK)
        if len(data) == 0:
            return False
    except BlockingIOError:
        return True  # socket is open and reading from it would block
    except ConnectionResetError:
        return False  # socket was closed for some other reason
    return True


def get_var(name: str, data: dict, root: dict) -> object:
    """Get data variable from a given SCPI name."""
    names = name.split(":")
    curr, rest = names[0], names[1:]
    if curr not in data:
        ok = False
        for key in data:
            if key.startswith(curr):
                curr = key
                ok = True
                break
        if not ok:
            print(f" x `{curr}` key does not exists")
            return None
    var = data[curr]
    if not rest:
        if callable(var):
            return var(root)
        return var
    return get_var(":".join(rest), var, root)


def set_var(name: str, strval: str, data: dict) -> bool:
    """Set variable using a given SCPI name."""
    names = name.split(":")
    curr, rest = names[0], names[1:]
    if curr not in data:
        ok = False
        for key in data:
            if key.startswith(curr):
                curr = key
                ok = True
                break
        if not ok:
            print(f" x `{curr}` key does not exists")
            return False
    if not rest:
        if callable(data[curr]):
            return False
        data[curr] = type(data[curr])(strval)
        return True
    return set_var(":".join(rest), strval, data[curr])


def __client__(client, dev_data):
    """Manage a client connection."""
    while is_connected(client):
        msg = client.recv(1024)
        if msg:
            msg = msg.decode("utf-8").strip()
            msg = msg.replace("\n", " ").replace("\t", " ")
            if msg[-1] in [chr(0), " "]:
                msg = msg[:-1]
            msg = msg.lower()
            commands = msg.split(";")
            for command in commands:
                if not command:
                    continue
                cmd = list(el for el in command.split(" ") if el)
                if not cmd:
                    print("no command found")
                    continue
                if cmd[0][-1] == "?":
                    if len(cmd) > 1:
                        print(" > syntax error ")
                        continue
                    value = get_var(cmd[0][:-1], dev_data, root=dev_data)
                    if value is not None:
                        client.send(f"{value}".encode("utf-8"))
                elif len(cmd) == 1:
                    print(" > command [not supported]")
                elif len(cmd) == 2:
                    print(f" >> SET {cmd[0]} {cmd[1]}")
                    set_var(cmd[0], cmd[1], dev_data)
                else:
                    print(f" >>> What {cmd} ??")
    print("Client disconnected")


def __args__():
    """Parse arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="YAML virtual instrument configuration")
    parser.add_argument(
        "-p", "--port", help="Set LXI port (default=5025)", type=int, default=5025
    )
    parser.add_argument(
        "-a",
        "--adress",
        help="Set LXI ip address (default=127.0.0.1)",
        type=str,
        default="127.0.0.1",
    )
    return parser.parse_args()


def __conv_data__(item: dict) -> object:
    """Convert dictionary to object."""
    if item["dtype"] == "func":
        func = item["func"]
        regex = r"\{(?P<var>[a-z|A-Z|:]+)\}"
        subst = 'get_var("\\g<var>", x, x)'
        result = "lambda x: " + re.sub(regex, subst, func, 0, re.MULTILINE)
        return eval(result)
    return None


def __prepare_data__(data: dict) -> dict:
    """Prepare data."""
    res = {}
    for key, item in data.items():
        if isinstance(item, dict):
            if "dtype" in item:
                res[key] = __conv_data__(item)
            else:
                res[key] = __prepare_data__(item)
        else:
            res[key] = item
    return res


def load_data(confpath: str) -> dict:
    """Load device configuration from yaml file."""
    with open(confpath, "r", encoding="utf-8") as conf:
        data = yaml.load(conf.read(), Loader=yaml.FullLoader)
        data = __prepare_data__(data)
        return data


def main():
    """Load and execute service."""
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    args = __args__()

    print(f"Starting servie at {args.port}")
    server.bind((args.adress, args.port))
    server.listen(1)
    dev_data = load_data(args.config)

    while True:
        client, (host, port) = server.accept()
        print(f"Client {host}:{port} connected")
        thread = threading.Thread(target=__client__, args=(client, dev_data))
        thread.start()


if __name__ == "__main__":
    main()
