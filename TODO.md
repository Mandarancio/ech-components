# To Do
 - [x] Move trigger checks to `Synchronize` method.
 - [x] `FORCE` scan mode non blocking.
 - [x] `driverStatus` common signal exposed.
 - [x] Ensure `Timeout`, `MaxTry` and `Readback` settings are implemented correctly.
 - [-] More documentation and examples.
 - [x] Add optional global and command delay.
 - [x] Increase readibility of `Execute` method.
 - [ ] Discuss about command types (CONSTANT)
