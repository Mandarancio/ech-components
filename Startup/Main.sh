#!/bin/bash
#Arguments -f FILENAME -m MESSAGE | -s STATE [-d cgdb|strace]

MDS=0
#DEBUG="cgdb"
#DEBUG=""
#DEBUG="gdb"
INPUT_ARGS=$*
while test $# -gt 0; do
	case "$1" in
	-d | --debug)
		DEBUG="$2"
		;;
	-mds)
		MDS=1

		;;
	esac
	shift
done

if [ -z ${MARTe2_DIR+x} ]; then
	MARTe2_DIR=$(
		cd ../../MARTe2-dev/
		pwd
	)
fi
if [ -z ${MARTe2_Components_DIR+x} ]; then
	export MARTe2_Components_DIR=$(
		cd ../../MARTe2-components/
		pwd
	)
fi

if [ -z ${ECH_Components_DIR+x} ]; then
	export ECH_Components_DIR=$(
		cd ../
		pwd
	)
fi

BUILD_PATH=Build/x86-linux
DSPATH=$BUILD_PATH/Components/DataSources
GPATH=$BUILD_PATH/Components/GAMs
IPATH=$BUILD_PATH/Components/Interfaces

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_DIR/$BUILD_PATH/Core/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/LinuxTimer/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/LoggerDataSource/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/FileDataSource/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSAPTH/LinkDataSource/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/UDP/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/RealTimeThreadSynchronisation/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$DSPATH/RealTimeThreadAsyncBridge/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/ConstantGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/ConversionGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/IOGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/FilterGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/HistogramGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/SSMGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/TriggerOnChangeGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$GPATH/WaveformGAM/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$IPATH/BaseLib2Wrapper/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$IPATH/MemoryGate/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARTe2_Components_DIR/$IPATH/SysLogger/

# Include ECH Components and Dependencies
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SCPI_DIR/Build/x86-linux/Drivers/SCPIDRV/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ECH_Components_DIR/$DSPATH/SCPI/

echo $LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH

if [ "$DEBUG" = "cgdb" ]; then
	cgdb --args $MARTe2_DIR/Build/x86-linux/App/MARTeApp.ex $INPUT_ARGS
elif [ "$DEBUG" = "gdb" ]; then
	gdb --args $MARTe2_DIR/Build/x86-linux/App/MARTeApp.ex $INPUT_ARGS
else
	$MARTe2_DIR/Build/x86-linux/App/MARTeApp.ex $INPUT_ARGS
fi
